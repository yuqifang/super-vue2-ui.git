const path = require('path')
const os = require('os')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')

function resolve (dir) {
  return path.join(__dirname, dir)
}
let localhost = ''
try {
  const network = os.networkInterfaces()
  Object.keys(network).forEach(function (item) {
    if (item === '以太网') {
      localhost = network[item][1].address
    }
  })
} catch (e) {
  localhost = 'localhost'
}

module.exports = {
  productionSourceMap: false,
  publicPath: './',
  outputDir: 'dist',
  assetsDir: 'assets',
  pages: {
    index: {
      entry: 'src/index.js',
      template: 'public/index.html',
      filename: 'index.html',
      title: '超级UI',
      chunks: ['chunk-vendors', 'chunk-common', 'index']
    }
  },
  css: {
    extract: true, // true: 将css 分离，false：将css打入js
    sourceMap: false // 开启 CSS source maps?
  },
  filenameHashing: false,
  chainWebpack: config => {
    config.resolve.alias.set('@', resolve('src'))
    // 移除prefetch插件，避免加载多余的资源
    config.plugins.delete('prefetch')
  },
  configureWebpack: config => {
    // 将 optimize-css-assets-webpack-plugin 插件插入到 webpack 配置中
    config.plugins.push(
      new OptimizeCSSAssetsPlugin({
        cssProcessorOptions: {
          safe: true,
          discardComments: { removeAll: true }
        },
        canPrint: false // 是否在控制台打印信息
      })
    )
  },
  devServer: {
    open: true,
    host: localhost,
    port: 8080,
    https: false,
    hotOnly: false
  }
}
