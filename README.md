# simple-treasure-xiang

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# 重要信息
node版本： v14.21.3

# package.json中添加引用入口及配置打包命令
    "main": "./super-xc-ui/super-xc-ui.common.js", // 入口配置
    "scripts": {
        "package": "vue-cli-service build --target lib ./src/package/index.js --name super-xc-ui --dest super-xc-ui"
    }
# 打包命令解释：
    --target lib 关键字 指定打包的目录
    --name 打包后的文件名字
    --dest 打包后的文件夹的名称

# 注意package.json中 main 属性的配置
    "main": "./super-xc-ui/super-xc-ui.common.js",

# 设置npm源为：npm config set registry=https://registry.npmjs.org

# 在业务代码入口中这么使用
    import superVue2Ui from 'super-xc-ui'
    import 'super-xc-ui/super-xc-ui/super-xc-ui.css'
    Vue.use(superVue2Ui)





# 一般这么配置

# package.json中添加引用入口及配置打包命令
    "main": "./dist/super-xc-ui.common.js", // 入口配置
    "scripts": {
        "package": "vue-cli-service build --target lib ./src/package/index.js --name super-xc-ui --dest dist"
    }
# 打包命令解释：
    --target lib 关键字 指定打包的目录
    --name 打包后的文件名字
    --dest 打包后的文件夹的名称

# 注意package.json中 main 属性的配置
    "main": "./dist/super-xc-ui.common.js",

# 设置npm源为：npm config set registry=https://registry.npmjs.org

# 在业务代码入口中这么使用
    import superVue2Ui from 'super-xc-ui'
    import 'super-xc-ui/dist/super-xc-ui.css'
    Vue.use(superVue2Ui)

    

# 在业务代码逻辑中可直接这么使用
    <super-button></super-button>

# 将库发布到npm上
npm publish --registry https://registry.npmjs.org/