// package/index.js
import SuperButton from '@/package/super-button/index.vue'
import SuperInput from '@/package/super-input/index.vue'
import SuperNoData from '@/package/super-no-data/index.vue'

// 将组件放在数组里
const coms = [
  SuperButton,
  SuperInput,
  SuperNoData
]

// 批量组件注册
const install = function (Vue) {
  coms.forEach((com) => {
    Vue.component(com.name, com)
  })
}

// 这个方法以后在使用的时候可以被Vue.use("包名")调用
export default install
