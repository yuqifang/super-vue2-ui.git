import Vue from 'vue'
import Router from 'vue-router'
import SuperButton from '@/package/super-button/index.vue'
import SuperInput from '@/package/super-input/index.vue'
import SuperNoData from '@/package/super-no-data/index.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'SuperButton',
      component: SuperButton
    },
    {
      path: '/super-button',
      name: 'SuperButton',
      component: SuperButton
    },
    {
      path: '/super-input',
      name: 'SuperInput',
      component: SuperInput
    },
    {
      path: '/super-no-data',
      name: 'SuperNoData',
      component: SuperNoData
    }
  ]
})
