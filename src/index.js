import Vue from 'vue'
import Index from './Index.vue'
import store from './store'
import FastClick from 'fastclick'
import router from '@/router/index.js'
import '@/assets/css/common.less'
FastClick.attach(document.body)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(Index)
}).$mount('#app')
