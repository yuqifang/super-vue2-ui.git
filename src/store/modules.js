import dealmodule_credit from './modules/dealmodule_credit'
import dealmodule_common from './modules/dealmodule_common'
export default {
  dealmodule_credit,
  dealmodule_common
}
