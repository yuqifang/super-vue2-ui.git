export default {
  namespaced: true,
  state: {
    mobileType: '',
    phoneNum: ''
  },
  getters: {},
  mutations: {
    setMobileType (state, type) {
      state.mobileType = type
    },
    changePhoneNum (state, num) {
      state.phoneNum = num
    }
  },
  actions: {
    actionsPhoneType ({ commit, state }, payload) {
      commit('setMobileType', payload)
    }
  }
}
